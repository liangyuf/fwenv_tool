ARCH ?= x86_64

ifeq ($(ARCH),arm64)
CROSS_COMPILE = aarch64-linux-gnu-
else
CROSS_COMPILE =
endif

OBJ_PATH = ./objs/
LIB_PATH = 
TARGET_PATH = ./output
INC_PATH = ./include

CFLAGS += $(addprefix -I,$(INC_PATH)) 
#LDFLAGS += $(addprefix -L,$(LIB_PATH)) 
CFLAGS += -DUSE_HOSTCC 

CC = $(CROSS_COMPILE)gcc
LD = $(CROSS_COMPILE)gcc
STRIP = $(CROSS_COMPILE)strip

TARGET  = $(TARGET_PATH)/fw_printenv
LIBS    =  


EXCLUDE_FILE = 
SRC_DIR := $(shell find . -maxdepth 5 -type d)
SOURCES = $(foreach src_dir, $(SRC_DIR), $(filter-out $(EXCLUDE_FILE),$(wildcard $(src_dir)/*.c)))
OBJECTS = $(addprefix $(OBJ_PATH), $(patsubst %.c, %.o, $(SOURCES)))

$(shell mkdir -p $(TARGET_PATH)) 
$(shell mkdir -p $(OBJ_PATH)) 

all : $(TARGET)	
$(TARGET):$(OBJECTS)
	$(LD) $(OBJECTS) $(LDFLAGS) $(addprefix -l,$(LIBS)) -o $(TARGET)
#	$(qexec)$(STRIP) --strip-unneeded $@

$(OBJ_PATH)%.o : %.c
	@mkdir -p $(dir $@);
#	@echo $*
	$(CC) -c $(CFLAGS) -o $(OBJ_PATH)$*.o $< 

.PHONY : clean
clean :
	rm -rf $(OBJ_PATH)
	rm -rf $(TARGET)





