u-boot fw_printenv/fw_setenv tools

将u-boot中的环境变量固件生成工具fw_printenv从u-boot源码中提取出来，方便单独移植到其他平台，以便独立制作u-boot环境变量固件用来烧录板卡。  
fw_env.h头文件可以根据自己的需要进行修改。

# 版本信息
u-boot v2019.2



